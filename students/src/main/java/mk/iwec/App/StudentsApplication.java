package mk.iwec.App;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import mk.iwec.controller.StudentController;
import mk.iwec.model.Student;
import mk.iwec.service.StudentServiceImpl;

@SpringBootApplication
@ComponentScan(basePackages = {"mk.iwec.controller","mk.iwec.service"})
public class StudentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsApplication.class, args);
	}
}
