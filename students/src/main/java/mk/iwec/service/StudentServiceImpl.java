package mk.iwec.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import mk.iwec.exceptions.StudentNotFoundException;
import mk.iwec.model.Student;
@Service
public class StudentServiceImpl implements StudentService<Student> {
	private Map<Integer, Student> students = new HashMap<>();

	@Override
	public Map<Integer, Student> findAll() {

		return students;

	}
	@Override
	public int insert(Student st) {
		int affected = 0;
		if (st == null || st.getFirstname() == null || st.getLastname() == null) {
			return 0;
		} else {
			students.put(students.size() + 1, st);
			affected++;
		}
		return affected;
	}

	@Override
	public Map.Entry<Integer, Student> findById(Integer id) {

		return students.entrySet().stream().filter(e -> e.getKey().equals(id)).findAny().orElseThrow(()->new StudentNotFoundException(id));
	}

	

	@Override
	public int update(Integer id, Student st) {
		int affected = 0;
		if (st == null || st.getFirstname() == null || st.getLastname() == null) {
			return 0;
		} else {
			students.replace(id, st);
			affected++;
		}

		return affected;
	}

	@Override
	public int deleteById(Integer id) {
		int affected = 0;
		if (!students.containsKey(id)) {
			return 0;
		} else {
			students.remove(id);
			affected++;
		}
		return affected;
	}

}
