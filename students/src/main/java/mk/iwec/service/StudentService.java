package mk.iwec.service;

import java.util.Map;
import java.util.Optional;

import mk.iwec.model.Student;

public interface StudentService<T> {
	
	public Map<Integer, Student> findAll();
	
	public int insert(T o);
	
	public Map.Entry<Integer, T> findById(Integer id);

	public int update(Integer id, T o);

	public int deleteById(Integer id);

}
