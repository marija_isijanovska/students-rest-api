package mk.iwec.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import mk.iwec.exceptions.StudentNotFoundException;
import mk.iwec.model.Student;
import mk.iwec.repository.InMemoryDB;
import mk.iwec.repository.InMemoryDBImpl;
import mk.iwec.service.StudentService;
import mk.iwec.service.StudentServiceImpl;

@RestController
@RequestMapping("/students")
public class StudentController {
	@Autowired
	private StudentServiceImpl service;

	@GetMapping
	@ResponseBody
	public Map<Integer, Student> findAll() {
		return service.findAll();
	}

	@PostMapping
	Integer newStudent(@RequestBody Student st) {
		return service.insert(st);
	}

	@GetMapping("/{id}")
	Map.Entry<Integer, Student> st(@PathVariable Integer id) {

		return service.findById(id);
	}

	@PutMapping("/{id}")
	Integer replaceStudent(@RequestBody Student newStudent, @PathVariable Integer id) {

		return service.update(id, newStudent);
		
	}

	@DeleteMapping("/{id}")
	Integer deleteEmployee(@PathVariable Integer id) {
		return service.deleteById(id);
	}

}
