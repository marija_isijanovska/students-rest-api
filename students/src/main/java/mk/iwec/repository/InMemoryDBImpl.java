package mk.iwec.repository;
import mk.iwec.exceptions.StudentNotFoundException;
import mk.iwec.model.Student;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

@Repository
public class InMemoryDBImpl implements InMemoryDB<Student> {
	private Map<Integer, Student> students = new HashMap<>();

	@Override
	public Map<Integer, Student> findAll() {

		return students;

	}

	@Override
	public int insert(Student st) {
		int affected = 0;
		if (st == null || st.getFirstname() == null || st.getLastname() == null) {
			return 0;
		} else {
			students.put(students.size() + 1, st);
			affected++;
		}
		return affected;
	}

	@Override
	public Map.Entry<Integer, Student> findById(Integer id) {

		return students.entrySet().stream().filter(e -> e.getKey().equals(id)).findAny().orElseThrow(()->new StudentNotFoundException(id));
	}

	@Override
	public int update(Integer id, Student st) {
		int affected = 0;

		if (st == null || st.getFirstname() == null || st.getLastname() == null) {
			return 0;
		} else {
			students.replace(id, st);
			affected++;
		}

		return affected;
	}

	@Override
	public int deleteById(Integer id) {
		int affected = 0;
		if (!students.containsKey(id)) {
			return 0;
		} else {
			students.remove(id);
			affected++;
		}
		return affected;
	}

}
